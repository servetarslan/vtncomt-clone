function toppadding() {
    var topsize = $('header').height();
    $('main').css('padding-top', topsize);
}

function topSearchAction() {
    $('.top-search').focus(function () {
        $(this).addClass('is-active');
        $('.search-main i').addClass('is-active');
        if ($('.top-search').focusout(function () {
            if ($('.top-search').val() != "") {
                $(this).val('');
            }
            $('.top-search').removeClass('is-active');
            $('.search-main i').removeClass('is-active');
        }));
    });
}

function cookieMainHide() {
    $("#homeMainCookie").click(function () {
        $(".cookie-main").fadeOut(150, function () {
            $(this).remove();
        });
    });
}

function mainslide() {
    $('.slider').slick({
        dots: false,
        autoplay: true,
        infinite: false,
        centerMode: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth: false,


        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    centerMode: false,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    centerMode: false,
                    arrows: false,
                    dots: true

                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    centerMode: false,
                    arrows: false,
                    dots: true

                }
            }
        ]
    });
    $('.slider-nav > div').click(function () {
        $('.slider').slick('slickGoTo', $(this).index());
    })
}

function navOpen(){
    $('nav').mouseover(function(){
        $('.nav-menu').addClass('is-open');
        $(this).mouseout(function(){
            $('.nav-menu').removeClass('is-open');
        });
    });

}

function mbNav(){
    $('#mb-nav').click(function(){
        $('.mobile-nav-menu').addClass('is-open');
    });

    $('.mobile-header-close').click(function(){
        $('.mobile-nav-menu').removeClass('is-open');
    });

    /*sub-menu*/ 
    var index = '.sub-1 a';
    $(index).on('click', function(){
        $('.sub-1').removeClass('selected');
        $(index).parent().addClass('selected');
        
    });

    $('#backMenu').click(function(){
        console.log("tıklanmıyor");
        $('.sub-1').removeClass('selected');
    });

    $('#all_close').click(function(){
        $('.mobile-nav-menu').removeClass('is-open');
        $('.sub-1').removeClass('selected');
    });
}


$(document).ready(function () {
    topSearchAction();
    cookieMainHide();
    toppadding();
    mainslide();
    navOpen();
    mbNav();


});
